from setuptools import setup

setup(name = "issues",
      version = "0.0.1",
      py_modules = ["issues"],
      install_requires = ["click", "rich"],
      entry_points = {
          "console_scripts": ["issues = issues:cli"]
            })
