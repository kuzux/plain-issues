title: Basic Features
timestamp: 1685135327
id: 23adbbca65f3bdf83f6a
is_closed: False
author: Arda Cinar
---
# Basic Features

* Remove Issues
* List all issues
* List issues by id prefix
* Show issue by id prefix
* Open/Closed status
* Tagging issues
* Search by tag / status
* Commenting on issues

Arda Cinar
2023-05-27T00:08:47.443622

---
test comment

Arda Cinar
2023-05-27T11:41:15.881733

---
Looks like commenting also works

Arda Cinar
2023-05-27T11:42:28.783020
