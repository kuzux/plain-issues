#!/usr/bin/env python3
import click

import tempfile
import os
import datetime
import subprocess
import random

from dataclasses import dataclass

from rich.console import Console
from rich.markdown import Markdown

import glob
import shutil

NOW = datetime.datetime.now()

proc = subprocess.run(['git', 'config', 'user.name'], capture_output=True, text=True)
AUTHOR = proc.stdout.rstrip()

proc = subprocess.run(['git', 'config', 'core.editor'], capture_output=True, text=True)
EDITOR = proc.stdout.rstrip()
if EDITOR == "": EDITOR = "nvim"

GREP_TOOL = "grep"
if shutil.which("rg") is not None:
    GREP_TOOL = "rg"
elif shutil.which("ag") is not None:
    GREP_TOOL = "ag"
elif shutil.which("ack") is not None:
    GREP_TOOL = "ack"

def extract_title_from_markdown(md_source):
    first_line = md_source.split("\n")[0]
    if first_line.startswith("#"):
        return first_line[1:].strip()
    return ""

@dataclass
class Issue:
    id: str
    timestamp: int
    author: str
    body: str
    title: str
    is_closed: bool

    @classmethod
    def create(cls):
        id = "{:x}".format(random.getrandbits(4*20))
        formatted_date = NOW.isoformat()

        body = f"# \n\n{AUTHOR}\n{formatted_date}\n"
        return cls(id = id,
                   title = "",
                   timestamp = int(NOW.timestamp()),
                   author = AUTHOR,
                   body = body,
                   is_closed = False)

    @classmethod
    def parse_markdown(cls, path):
        source = ""
        with open(path, "r") as fp:
            source = fp.read()
        parts = source.split("---\n", 1)
        if len(parts) < 2: return None

        metadata = dict()

        for line in parts[0].split("\n"):
            line_parts = line.split(":")
            if len(line_parts) != 2: continue
            key = line_parts[0].strip()
            value = line_parts[1].strip()

            metadata[key] = value

        body = parts[1]

        return cls(id = metadata['id'],
                   title = extract_title_from_markdown(body),
                   timestamp = int(metadata['timestamp']),
                   author = metadata['author'],
                   body = body,
                   is_closed = bool(metadata['is_closed']))

    def write_out(self):
        metadata = {
                "title": self.title,
                "timestamp": self.timestamp,
                "id": self.id,
                "is_closed": self.is_closed,
                "author": self.author
            }

        os.makedirs("__issues__/closed", exist_ok=True)
        open_path = f"__issues__/{self.id}.md"
        closed_path = f"__issues__/closed/{self.id}.md"

        # TODO: We should probably write the new file first, and then remove it afterwards
        issue_path = ""
        if self.is_closed:
            if os.path.exists(open_path): os.remove(open_path)
            issue_path = closed_path
        else:
            if os.path.exists(closed_path): os.remove(closed_path)
            issue_path = open_path

        with open(issue_path, "w") as fp:
            for key, value in metadata.items():
                fp.write(f"{key}: {value}\n")
            fp.write("---\n")
            fp.write(self.body)

    def add_comment(self, markdown_body):
        self.body += "\n---\n"
        self.body += markdown_body

@click.group()
def cli():
    pass

@cli.command()
def create():
    issue = Issue.create()
    fd, tmp_path = tempfile.mkstemp(suffix='new_issue.md', text=True)
    with os.fdopen(fd, "w") as fp:
        fp.write(issue.body)
    
    os.system(f"{EDITOR} {tmp_path}")
    
    with open(tmp_path) as fp:
        markdown_src = fp.read()
        issue.body = markdown_src
        issue.title = extract_title_from_markdown(issue.body)

    os.remove(tmp_path)
    issue.write_out()
    click.echo(f"Created issue {issue.id}")


@cli.command()
@click.argument('id_prefix')
def comment(id_prefix):
    path = path_from_id_prefix(id_prefix)
    issue = Issue.parse_markdown(path)

    body = f"\n\n{AUTHOR}\n{NOW.isoformat()}\n"

    fd, tmp_path = tempfile.mkstemp(suffix='new_comment.md', text=True)
    with os.fdopen(fd, "w") as fp:
        fp.write(body)

    os.system(f"nvim {tmp_path}")
    
    with open(tmp_path) as fp:
        markdown_src = fp.read()
        issue.add_comment(markdown_src)

    os.remove(tmp_path)
    issue.write_out()
    
def path_from_id_prefix(id_prefix):
    files = glob.glob(f"__issues__/{id_prefix}*.md")
    files += glob.glob(f"__issues__/closed/{id_prefix}*.md")

    if len(files) == 0:
        click.echo("No issues found with that id")
        raise SystemExit(1)
    elif len(files) > 1:
        click.echo("Found multiple matching issues")
        for issue_path in files:
            no_dir = issue_path.split('/')[-1]
            no_ext = no_dir.split('.')[0]
            click.echo(no_ext)
        raise SystemExit(1)
    return files[0]

@cli.command()
@click.argument('id_prefix')
def close(id_prefix):
    path = path_from_id_prefix(id_prefix)
    issue = Issue.parse_markdown(path)
    issue.is_closed = True
    issue.write_out()

@cli.command()
@click.argument('id_prefix')
def reopen(id_prefix):
    path = path_from_id_prefix(id_prefix)
    issue = Issue.parse_markdown(path)
    issue.is_closed = False
    issue.write_out()

@cli.command()
@click.argument('id_prefix')
def show(id_prefix):
    path = path_from_id_prefix(id_prefix)
    console = Console()
    issue = Issue.parse_markdown(path)
    console.print(Markdown(issue.body))

@cli.command()
@click.option('-a', '--all/--open', default=False, help="List closed issues as well")
def list(all):
    files = glob.glob("__issues__/*.md")
    if all:
        files += glob.glob("__issues__/closed/*.md")
    issues = []

    for issue_path in files:
        issues.append(Issue.parse_markdown(issue_path))

    issues.sort(key=lambda i: i.timestamp, reverse=True)
    for issue in issues:
        click.echo(f"{issue.id}\t{issue.title}")

@cli.command()
@click.argument('pattern')
@click.option('-a', '--all/--open', default=False, help="Search closed issues as well")
def search(pattern, all):
    files = "__issues__/*.md"
    if all: files = "__issues__/*.md __issues__/closed/*.md"

    if GREP_TOOL == "grep":
        click.echo("No search tool installed. Falling back to grep")

    cmdline = f"{GREP_TOOL} -n -e \"{pattern}\" -- {files}"
    subprocess.run(cmdline, shell=True, stderr=subprocess.DEVNULL)

if __name__ == "__main__":
    cli()
